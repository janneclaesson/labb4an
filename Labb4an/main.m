//
//  main.m
//  Labb4an
//
//  Created by IT-Högskolan on 2015-02-20.
//  Copyright (c) 2015 Janne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
