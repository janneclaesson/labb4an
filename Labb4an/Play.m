//
//  Play.m
//  Labb4an
//
//  Created by IT-Högskolan on 2015-02-20.
//  Copyright (c) 2015 Janne. All rights reserved.
//
 
#import "Play.h"

@interface Play ()

@end
@implementation Play


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *Drag = [[event allTouches]anyObject];
    Player.center = [Drag locationInView:self.view];
    
    if (Player.center.y > 639){
        Player.center = CGPointMake(Player.center.x,639);
    }
    if (Player.center.y < 639){
        Player.center = CGPointMake(Player.center.x,639);
    }
    if(Player.center.x < 30) {
        Player.center = CGPointMake(30, Player.center.y);
    }
    if(Player.center.x > 330) {
        Player.center = CGPointMake(330, Player.center.y);
    }
    
}

-(void) OpponentMoves{
    
    if(Opponent.center.x > Ball.center.x){
        Opponent.center = CGPointMake(Opponent.center.x - 2, Opponent.center.y);
    }
    if(Opponent.center.x < Ball.center.x){
        Opponent.center = CGPointMake(Opponent.center.x + 2, Opponent.center.y);
    }
    if(Opponent.center.x < 30) {
        Opponent.center = CGPointMake(30, Opponent.center.y);
    }
    if(Opponent.center.x > 308) {
        Opponent.center = CGPointMake(308, Opponent.center.y);
    }
}


- (IBAction)StartButton:(id)sender {
    
    StartButton.hidden = YES;
    
    
    Y = arc4random() %11;
    Y = Y -5;
    
    X = arc4random() %11;
    X = X -5;
    
    if (Y == 0) {
        Y = 1;
    }
    if (X == 0) {
        X = 1;
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(BallMoves)
                                           userInfo:nil repeats:YES];
}

-(void)Hit{
    if (CGRectIntersectsRect(Ball.frame, Player.frame)){
        Y = arc4random() %5;
        Y = 0-Y;
    }
    if (CGRectIntersectsRect(Ball.frame, Opponent.frame)){
        Y = arc4random() %5;
        
    }
    
}
-(void) BallMoves{
    
    [self OpponentMoves];
    [self Hit];
    Ball.center = CGPointMake(Ball.center.x + X, Ball.center.y + Y);
    
    if (Ball.center.x < -0) {
        X = 0 - X;
    }
    if (Ball.center.x > 370) {
        X = 0 - X;
    }
    if (Ball.center.y < 0){
        PlayerScoreNum = PlayerScoreNum +1;
        PlayerScore.text = [NSString stringWithFormat:@"%i",PlayerScoreNum];
        [timer invalidate];
        StartButton.hidden = NO;
        
        Ball.center = CGPointMake(144, 273);
        Opponent.center = CGPointMake(160, 32);
        Player.center = CGPointMake(160, 32);
        
        
        if(PlayerScoreNum == 10){
            StartButton.hidden = YES;
            
            Result.hidden = NO;
            Result.text = [NSString stringWithFormat:@"Du vann!"];
        }
        
    }
    if (Ball.center.y > 650){
        OpponentScoreNum = OpponentScoreNum +1;
        OpponentScore.text = [NSString stringWithFormat:@"%i",OpponentScoreNum];
        [timer invalidate];
        StartButton.hidden = NO;
        
        
        Ball.center = CGPointMake(144, 273);
        Opponent.center = CGPointMake(160, 32);
        Player.center = CGPointMake(160, 32);
        
        if(OpponentScoreNum == 10){
            StartButton.hidden = YES;
            
            Result.hidden = NO;
            Result.text = [NSString stringWithFormat:@"Looser!"];
        }
    }
}

- (void)viewDidLoad {
    
    PlayerScoreNum = 0;
    OpponentScoreNum = 0;
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
