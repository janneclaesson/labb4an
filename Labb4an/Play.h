//
//  Play.h
//  Labb4an
//
//  Created by IT-Högskolan on 2015-02-20.
//  Copyright (c) 2015 Janne. All rights reserved.
//
//
 #import <UIKit/UIKit.h>

int Y;
int X;

int OpponentScoreNum;
int PlayerScoreNum;


@interface Play : UIViewController
{
    IBOutlet UIImageView *Ball;
    IBOutlet UIButton *StartButton;
    IBOutlet UIImageView *Player;
    IBOutlet UIImageView *Opponent;
    
    IBOutlet UILabel *PlayerScore;
    IBOutlet UILabel *OpponentScore;
    
    IBOutlet UILabel *Result;
    
    
    NSTimer *timer;
}
-(IBAction) StartButton:(id)sender;
-(void) BallMoves;
-(void) OpponentMoves;
-(void) Hit;
@end
